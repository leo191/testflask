from flask import Flask
app = Flask(__name__)

@app.route("/")
def index():
	return "I'm on index \n"

@app.route("/member/<string:name>")
def getmember(name):
	return name

@app.route("/hello")
def hello():
    return "Hello World!\n"

@app.route("/update")
def updated():
	return "Your site has been updated successfully"


#if "__name__"=="__main__":
app.run(host='0.0.0.0',port=5000)
